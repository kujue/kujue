<?php
/**
 * 执行程序
 */

if ($action == 'callback') {
    // 表示回调返回
    if (isset($_REQUEST['code'])) {
        // 获取access_token
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$appkey.'&code='.$_REQUEST['code'].'&grant_type=authorization_code';
        $token = json_decode(dr_catcher_data($url), true);
        if (!$token) {
            $this->_msg(0, dr_lang('无法获取到远程信息'));
        } elseif ($token['errmsg']) {
            $this->_msg(0, $token['errmsg']);
        }
        // 获取用户信息
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$token['access_token'].'&openid='.$token['openid'];
        $user = json_decode(dr_catcher_data($url), true);
        if (!$user) {
            $this->_msg(0, dr_lang('无法获取到用户信息'));
        } elseif ($user['errmsg']) {
            $this->_msg(0, $user['errmsg']);
        }
        $rt = \Phpcmf\Service::M('member')->insert_oauth($this->uid, $type, [
            'oid' => $token['openid'],
            'oauth' => 'weixin',
            'avatar' => $user['headimgurl'],
            'unionid' => (string)$user['unionid'],
            'nickname' => dr_emoji2html($user['nickname']),
            'expire_at' => SYS_TIME,
            'access_token' => 0,
            'refresh_token' => $token['refresh_token'],
        ], null,  $back);
        if (!$rt['code']) {
            $this->_msg(0, $rt['msg']);exit;
        } else {
            dr_redirect($rt['msg']);
        }
    } else {
        $this->_msg(0, dr_lang('回调参数code不存在'));exit;
    }
} else {
    // 跳转授权页面
    $url = 'https://open.weixin.qq.com/connect/qrconnect?appid='.$appid.'&redirect_uri='.urlencode($callback_url).'&response_type=code&scope=snsapi_login&state=STATE#wechat_redirect';
    dr_redirect($url);
}