<?php namespace Phpcmf\Field;
/**
 * https://kujue.cc
 * 酷觉
 * 本文件是框架系统文件，二次开发时不可以修改本文件
 **/

class Quanxianzu extends \Phpcmf\Library\A_Field  {

	/**
	 * 字段相关属性参数
	 */
	public function option($option, $field = null) {
        return ['', '<div class="form-group">
			<label class="col-md-2 control-label">'.dr_lang('显示游客组').'</label>
			<div class="col-md-9">
				<input type="checkbox" name="data[setting][option][guest]" '.($option['guest'] ? 'checked' : '').' value="1"  data-on-text="'.dr_lang('已开启').'" data-off-text="'.dr_lang('已关闭').'" data-on-color="success" data-off-color="danger" class="make-switch" data-size="small">

				<span class="help-block">'.dr_lang('把游客组作为一个用户组来显示').'</span>
			</div>
		</div>'];
	}

    /**
     * 创建sql语句
     */
    public function create_sql($name, $value, $cname = '') {
        $sql = 'ALTER TABLE `{tablename}` ADD `'.$name.'` varchar(255) NULL , ADD `'.$name.'_sku` TEXT NULL, ADD `'.$name.'_rmb` TEXT NULL, ADD `'.$name.'_jifen` TEXT NULL';
        return $sql;
    }

    /**
     * 修改sql语句
     */
    public function alter_sql($name, $value, $cname = '') {
        return NULL;
    }

    /**
     * 删除sql语句
     */
    public function drop_sql($name) {
        $sql = 'ALTER TABLE `{tablename}` DROP `'.$name.'`, DROP `'.$name.'_sku`, DROP `'.$name.'_rmb`, DROP `'.$name.'_jifen`';
        return $sql;
    }

    // 测试字段是否被创建成功，默认成功为0，需要继承开发
    public function test_sql($tables, $field) {

        if (!$tables) {
            return 0;
        }

        foreach ($tables as $table) {
            if (!\Phpcmf\Service::M()->db->fieldExists($field.'_sku', $table)) {
                return '给表['.$table.']创建字段['.$field.'_sku'.']失败';
            }
            if (!\Phpcmf\Service::M()->db->fieldExists($field.'', $table)) {
                return '给表['.$table.']创建字段['.$field.''.']失败';
            }
			if (!\Phpcmf\Service::M()->db->fieldExists($field.'_rmb', $table)) {
			    return '给表['.$table.']创建字段['.$field.'_rmb'.']失败';
			}
			if (!\Phpcmf\Service::M()->db->fieldExists($field.'_jifen', $table)) {
			    return '给表['.$table.']创建字段['.$field.'_jifen'.']失败';
			}
        }

        return 0;
    }

    /**
     * 字段入库值
     */
	
    public function insert_value($field) {
        $quanid = (int)$_POST['is_field_'.$field['fieldname']];
        //dr_debug('quanid', $quanid);
        if ($quanid==1) {
            // 用户组
            $sku = $_POST['data'][$field['fieldname'].'_sku'];
            $price = min($sku);
            \Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname']] = '';
            \Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_sku'] = dr_array2string($sku);
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_rmb'] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_jifen'] = '';
        } else if ($quanid==2) {
			// 用户组
			$rmb = $_POST['data'][$field['fieldname'].'_rmb'];
			$price = min($rmb);
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname']] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_sku'] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_rmb'] = dr_array2string($rmb);
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_jifen'] = '';
		} else if ($quanid==3) {
			// 用户组
			$jifen = $_POST['data'][$field['fieldname'].'_jifen'];
			$price = min($jifen);
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname']] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_sku'] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_rmb'] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_jifen'] = dr_array2string($jifen);
		}
		 else {
            // 单一
            \Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname']] = $_POST['data'][$field['fieldname']];
            \Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_sku'] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_rmb'] = '';
			\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname'].'_jifen'] = '';
        }

    }

    /**
     * 字段值
     */
    public function get_value($name, $data) {

    }

    /**
     * 字段输出
     *
     * @param	array	$value	值
     * @return  string
     */
    public function output($value) {
        return $value;
    }

    /**
     * 字段表单输入
     *
     * @return  string
     */
    public function input($field, $value = []) {

        // 字段显示名称
        $text = ($field['setting']['validate']['required'] ? '<span class="required" aria-required="true"> * </span>' : '').dr_lang($field['name']);

        // 字段提示信息
        $tips = $field['setting']['validate']['tips'] ? '<span class="help-block" id="dr_'.$field['fieldname'].'_tips">'.$field['setting']['validate']['tips'].'</span>' : '';

        // 模式
		$skus = \Phpcmf\Service::L('Field')->value[$field['fieldname'].''];
        $sku = dr_string2array(\Phpcmf\Service::L('Field')->value[$field['fieldname'].'_sku']);
		$rmb = dr_string2array(\Phpcmf\Service::L('Field')->value[$field['fieldname'].'_rmb']);
		$jifen = dr_string2array(\Phpcmf\Service::L('Field')->value[$field['fieldname'].'_jifen']);
        $html = '';
		$htmlhuiyuan = '';
		$htmlrmb = '';
		$htmljifen = '';
        if ($field['setting']['option']['guest']) {
            $group = dr_array22array([
                0 => [
                    'id' => 0,
                    'name' => dr_lang('游客'),
                ],
            ], \Phpcmf\Service::C()->member_cache['group']);
        } else {
            $group = \Phpcmf\Service::C()->member_cache['group'];
        }
        foreach ($group as $g) {
            $html.= '<label><div class="input-group">
			<span class="input-group-addon " >'.$g['name'].'</span>
			<input type="text" class="form-control" name="data['.$field['fieldname'].'_sku]['.$g['id'].']" value="'.(string)$sku[$g['id']].'" /> 
			</div></label>';
        }
		foreach ($group as $g) {
			$htmlrmb.= '<label><div class="input-group">
			<span class="input-group-addon " >'.$g['name'].'</span>
			<input type="text" class="form-control" name="data['.$field['fieldname'].'_rmb]['.$g['id'].']" value="'.(string)$rmb[$g['id']].'" /> 
			</div></label>';
		}
		foreach ($group as $g) {
			$htmljifen.= '<label><div class="input-group">
			<span class="input-group-addon " >'.$g['name'].'</span>
			<input type="text" class="form-control" name="data['.$field['fieldname'].'_jifen]['.$g['id'].']" value="'.(string)$jifen[$g['id']].'" /> 
			</div></label>';
		}
		foreach ($group as $g) {
		            $htmlhuiyuan.= '<div class="">
			<div class="col-xs-3"><span >'.$g['name'].'</span></div>
			<div class="col-xs-8">
			<label class="mt-radio col-xs-5">
<input type="radio"  name="data['.$field['fieldname'].'_sku]['.$g['id'].']" value="1"  '.($sku[$g['id']]==1 ? 'checked' : '').'/> 无权 <span></span> 
</label>
<label class="mt-radio col-xs-5">
<input type="radio"  name="data['.$field['fieldname'].'_sku]['.$g['id'].']" value="0" '.($sku[$g['id']]==0 ? 'checked' : '').'/> 有权 <span></span> 
</label></div></div><br>';
		        }
		$is_fields = $skus? 1 : 0;
        $is_field = $sku? 1 : 0;
		$is_fieldrmb = $rmb? 1 : 0;
		$is_fieldjifen = $jifen? 1 : 0;

        $str = '
            <div class="mt-radio-inline">
                <label class="mt-radio">
                    <input type="radio" onclick="$(\'#dr_field_'.$field['fieldname'].'\').show();$(\'#dr_field_'.$field['fieldname'].'2\').hide();$(\'#dr_field_'.$field['fieldname'].'3\').hide();$(\'#dr_field_'.$field['fieldname'].'4\').hide();" name="is_field_'.$field['fieldname'].'" value="0" '.(!$is_field ? 'checked' : '').'> '.dr_lang('全局关闭').'
                    <span></span>
                </label>
                <label class="mt-radio" id="huiyuanquanxian1">
                    <input type="radio" onclick="$(\'#dr_field_'.$field['fieldname'].'2\').show();$(\'#dr_field_'.$field['fieldname'].'\').hide();$(\'#dr_field_'.$field['fieldname'].'3\').hide();$(\'#dr_field_'.$field['fieldname'].'4\').hide();" name="is_field_'.$field['fieldname'].'" value="1" '.($is_field ? 'checked' : '').'> '.dr_lang('会员组权限').'
                    <span></span>
                </label>
				<label class="mt-radio" id="huiyuanrmb2">
				    <input type="radio" onclick="$(\'#dr_field_'.$field['fieldname'].'3\').show();$(\'#dr_field_'.$field['fieldname'].'\').hide();$(\'#dr_field_'.$field['fieldname'].'2\').hide();$(\'#dr_field_'.$field['fieldname'].'4\').hide();" name="is_field_'.$field['fieldname'].'" value="2" '.($is_fieldrmb ? 'checked' : '').'> '.dr_lang('人民币权限').'
				    <span></span>
				</label>
				<label class="mt-radio" id="huiyuanjifen3">
				    <input type="radio" onclick="$(\'#dr_field_'.$field['fieldname'].'4\').show();$(\'#dr_field_'.$field['fieldname'].'\').hide();$(\'#dr_field_'.$field['fieldname'].'2\').hide();$(\'#dr_field_'.$field['fieldname'].'3\').hide();" name="is_field_'.$field['fieldname'].'" value="3" '.($is_fieldjifen ? 'checked' : '').'> '.dr_lang('积分权限').'
				    <span></span>
				</label>
            </div>
            <div id="dr_field_'.$field['fieldname'].'" style="display:'.($is_fields ? 'block' : 'none').';">
                
                <label class="mt-radio mt-radio-outline"><input  type="radio" name="data['.$field['fieldname'].']" id="dr_'.$field['fieldname'].'" value="0" '.($value==0 ? 'checked' : '').'/>关闭全局<span></span></label>
                <label class="mt-radio mt-radio-outline"><input  type="radio" name="data['.$field['fieldname'].']" id="dr_'.$field['fieldname'].'" value="1" '.($value==1 ? 'checked' : '').'/>免费开放<span></span></label>
			</div>
            <div id="dr_field_'.$field['fieldname'].'2" style="display:'.($is_field ? 'block' : 'none').';">
                <div style="width: 100%">
                '.$htmlhuiyuan.'
                </div>
            </div>
			<div id="dr_field_'.$field['fieldname'].'3" style="display:'.($is_fieldrmb ? 'block' : 'none').';">
			    <div style="width: 100%">
			    '.$htmlrmb.'
			    </div>
			</div>
			<div id="dr_field_'.$field['fieldname'].'4" style="display:'.($is_fieldjifen ? 'block' : 'none').';">
			    <div style="width: 100%">
			    '.$htmljifen.'
			    </div>
			</div>
            ';
        return $this->input_format($field['fieldname'], $text, $str.$tips);

	}

    /**
     * 字段表单显示
     */
    public function show($field, $value = null) {

        // 字段显示名称
        $text = ($field['setting']['validate']['required'] ? '<span class="required" aria-required="true"> * </span>' : '').$field['name'];

        // 字段提示信息
        $tips = $field['setting']['validate']['tips'] ? '<span class="help-block" id="dr_'.$field['fieldname'].'_tips">'.$field['setting']['validate']['tips'].'</span>' : '';

        // 模式
        $sku = dr_string2array(\Phpcmf\Service::L('Field')->value['score_sku']);
        $html = '';
        if ($field['setting']['option']['guest']) {
            $group = dr_array22array([
                0 => [
                    'id' => 0,
                    'name' => dr_lang('游客'),
                ],
            ], \Phpcmf\Service::C()->member_cache['group']);
        } else {
            $group = \Phpcmf\Service::C()->member_cache['group'];
        }
        foreach ($group as $g) {
            $html.= '<label><div class="input-group">
<span class="input-group-addon">'.$g['name'].'</span>
<input type="text" class="form-control"  readonly name="data['.$field['fieldname'].'_sku]['.$g['id'].']" value="'.(string)$sku[$g['id']].'" /> 
</div></label>';
        }

        $is_field = $sku? 1 : 0;

        $str = '
            <div id="dr_field_'.$field['fieldname'].'" style="display:'.(!$is_field ? 'block' : 'none').';">
                <label><input class="form-control " readonly type="text" name="data['.$field['fieldname'].']" id="dr_'.$field['fieldname'].'" value="'.$value.'" /></label>
            </div>
            <div id="dr_field_'.$field['fieldname'].'2" style="display:'.($is_field ? 'block' : 'none').';">
                <div style="width: 100%">
                '.$html.'
                </div>
            </div>
            ';
        return $this->input_format($field['fieldname'], $text, $str.$tips);

    }

}