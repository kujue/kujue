<?php namespace Phpcmf\Control\Api;
/**
 * kujue.cc
 * 酷觉内容管理框架系统（简称：酷觉CMS）
 * 本文件是框架系统文件，二次开发时不可以修改本文件
 **/

// 模块ajax操作接口
class Module extends \Phpcmf\Common {

    private $siteid;
    private $dirname;
    private $tablename;

    protected $content_model;

    public function __construct(...$params) {
        parent::__construct(...$params);
        // 初始化模块
        $this->siteid = (int)\Phpcmf\Service::L('input')->get('siteid');
        !$this->siteid && $this->siteid = SITE_ID;
        $this->dirname = dr_safe_filename(\Phpcmf\Service::L('input')->get('app'));
        if ($this->dirname == 'MOD_DIR') {
            $this->_msg(0, dr_lang('app参数存在问题'));
        } elseif (!$this->dirname || !dr_is_app_dir(($this->dirname))) {
            $this->_msg(0, dr_lang('模块目录[%s]不存在', $this->dirname));
            exit;
        }
        $this->tablename = $this->siteid.'_'.$this->dirname;
        $this->content_model = \Phpcmf\Service::M('Content', $this->dirname);
        $this->_module_init($this->dirname, $this->siteid);
    }

    public function index() {
        exit('module api');
    }

    /**
     * 阅读数统计
     */
    public function hits() {

        $id = (int)\Phpcmf\Service::L('input')->get('id');
        if (!$id) {
            $this->_jsonp(0, dr_lang('阅读统计: id参数不完整'));
        }

        $rt = $this->content_model->update_hits($id, (int)\Phpcmf\Service::L('input')->get('gx'));

        $this->_jsonp($rt['code'], $rt['msg'], $rt['data']);
    }

    /**
     * 收藏模块内容
     */
    public function favorite() {

        if (!dr_is_app('favorite')) {
            $this->_json(0, dr_lang('应用[模块内容收藏]未安装'));
        }

        $rt = \Phpcmf\Service::M('op', 'favorite')->run($this->tablename, $this->dirname);

        $this->_json($rt['code'], $rt['msg'], $rt['data']);
    }

    /**
     * 是否收藏模块内容
     */
    public function is_favorite() {

        if (!dr_is_app('favorite')) {
            $this->_json(0, dr_lang('应用[模块内容收藏]未安装'));
        }

        $rt = \Phpcmf\Service::M('op', 'favorite')->is_favorite($this->tablename, $this->dirname);

        $this->_json($rt['code'], $rt['msg'], $rt['data']);
    }

    /**
     * 模块内容支持与反对
     */
    public function digg() {

        if (!dr_is_app('zan')) {
            $this->_json(0, dr_lang('应用[模块内容点赞]未安装'));
        }

        $rt = \Phpcmf\Service::M('op', 'zan')->run($this->tablename, $this->dirname);

        $this->_json($rt['code'], $rt['msg'], $rt['data']);
    }
	/**
	 * 模块内容红蓝PK
	 */
	public function pk() {
	
	    if (!dr_is_app('pk')) {
	        $this->_json(0, dr_lang('应用[模块内容红蓝pk]未安装'));
	    }
	
	    $rt = \Phpcmf\Service::M('op', 'pk')->run($this->tablename, $this->dirname);
	
	    $this->_json($rt['code'], $rt['msg'], $rt['data']);
	}
	/**
	 * 模块内容红蓝PK
	 */
	public function pklan() {
	
	    if (!dr_is_app('pk')) {
	        $this->_json(0, dr_lang('应用[模块内容红蓝pk]未安装'));
	    }
	
	    $rt = \Phpcmf\Service::M('oplan', 'pk')->run($this->tablename, $this->dirname);
	
	    $this->_json($rt['code'], $rt['msg'], $rt['data']);
	}
	/**
	 * 模块流程
	 */
	public function liucheng() {
	
	    if (!dr_is_app('liucheng')) {
	        $this->_json(0, dr_lang('应用[模块流程]未安装'));
	    }
	
	    $rt = \Phpcmf\Service::M('op', 'liucheng')->run($this->tablename, $this->dirname);
	
	    $this->_json($rt['code'], $rt['msg'], $rt['data']);
	}
	/**
	 * 模块流程
	 */
	public function liuchengson() {
	
	    if (!dr_is_app('liucheng')) {
	        $this->_json(0, dr_lang('应用[模块流程]未安装'));
	    }
	
	    $rt = \Phpcmf\Service::M('son', 'liucheng')->run($this->tablename, $this->dirname);
	
	    $this->_json($rt['code'], $rt['msg'], $rt['data']);
	}
	/**
	 * 模块投票
	 */
	public function toupiao() {
	
	    if (!dr_is_app('toupiao')) {
	        $this->_json(0, dr_lang('应用[模块投票]未安装'));
	    }
	
	    $rt = \Phpcmf\Service::M('op', 'toupiao')->run($this->tablename, $this->dirname);
	
	    $this->_json($rt['code'], $rt['msg'], $rt['data']);
	}
	/**
	 * 模块内容订帖
	 */
	public function dingtie() {
	
	    if (!dr_is_app('dingtie')) {
	        $this->_json(0, dr_lang('应用[模块内容订帖]未安装'));
	    }
	
	    $rt = \Phpcmf\Service::M('op', 'dingtie')->run($this->tablename, $this->dirname);
	
	    $this->_json($rt['code'], $rt['msg'], $rt['data']);
	}
	/**
	 * 模块关注
	 */
	public function bankuai() {
	
	    if (!dr_is_app('guanzhubankuai')) {
	        $this->_json(0, dr_lang('应用[模块板块]未安装'));
	    }
	
	    $rt = \Phpcmf\Service::M('op', 'guanzhubankuai')->run($this->tablename, $this->dirname);
	
	    $this->_json($rt['code'], $rt['msg'], $rt['data']);
	}

}
