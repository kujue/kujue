<?php
// 此文件是安装授权文件，每次下载安装包会自动生成，请勿修改
return [

    'oem' => '280',
    'name' => '广州市盛想科技有限公司',
    'url' => 'https://kujue.cc',
    'domain' => 'https://kujue.cc/license',
    'cloud' => 'https://kujue.cc',
    'service' => 'https://kujue.cc/wendas',
    'license' => '60BB8062EA8E0C7FF17BB2E484CD228715',

];
