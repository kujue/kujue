<?php

/**
 * 开发者自定义函数文件
 */

// 获取内容中的iframe
function dr_get_content_iframe($value) {

    $rt = [];
    if (preg_match_all('/<iframe src="(.+)"/iU', $value, $imgs)) {
        $imgs[1] = array_unique($imgs[1]);
			foreach ($imgs[1] as $i => $img) {
				$rt[] = dr_file(trim($img, '"'));
			}
    }

    return $rt;
}
// 获取内容中的MP3
function dr_get_content_audio($value, $num = 0) {
    return dr_get_content_url($value, 'src', 'mp3', $num);
}
// 获取内容中的iframe
function dr_get_content_imgs($value) {

    $rt = [];
    if (preg_match_all('/<img data-kujue="(.+)" src="(.+)"/iU', $value, $imgs)) {
        $imgs[1] = array_unique($imgs[1]);
			foreach ($imgs[1] as $i => $img) {
				$rt[] = dr_file(trim($img, '"'));
			}
    }

    return $rt;
}