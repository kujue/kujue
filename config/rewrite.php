<?php

/**
 * URL解析规则
 * 例如：  114.html 对应 index.php?s=demo&c=show&id=114
 * 可以解析：  "114.html"  => 'index.php?s=demo&c=show&id=114',
 * 动态id解析：  "([0-9]+).html"  => 'index.php?s=demo&c=show&id=$1',
 */

return [

    /***********************下面写你自己的URL解析规则********************/
	

    /***********************共享栏目测试规则：栏目列表页面分页的伪静态解析*************************/
    "list\-([\w]+)\-([0-9]+).html(.*)"  => 'index.php?c=category&dir=$1&page=$2',

    /***********************共享栏目测试规则：栏目列表页面的伪静态解析*************************/
    "list\-([\w]+).html(.*)"  => 'index.php?c=category&dir=$1',

    /***********************共享栏目测试规则：内容页面分页的伪静态解析*************************/
    "show\-([0-9]+)\-([0-9]+).html(.*)"  => 'index.php?c=show&id=$1&page=$2',

    /***********************共享栏目测试规则：内容页面的伪静态解析*************************/
    "show\-([0-9]+).html(.*)"  => 'index.php?c=show&id=$1',

    "([a-z]+)" => "index.php?s=$1",  
    //【独立模块】模块首页（{modname}）
    "([a-z]+)\/list\/([0-9]+)\/([0-9]+)\.html" => "index.php?s=$1&c=category&id=$2&page=$3",  
    //【独立模块】模块栏目列表(分页)（{modname}/list/{id}/{page}.html）
    "([a-z]+)\/list\/([0-9]+)\.html" => "index.php?s=$1&c=category&id=$2",  
    //【独立模块】模块栏目列表（{modname}/list/{id}.html）
     "([a-z]+)\/show\/([0-9]+)\/([0-9]+)\.html" => "index.php?s=$1&c=show&id=$2&page=$3",  
     //【独立模块】模块内容页(分页)（{modname}/show/{id}/{page}.html）
     "([a-z]+)\/show\/([0-9]+)\.html" => "index.php?s=$1&c=show&id=$2",  
     //【独立模块】模块内容页（{modname}/show/{id}.html）
     "([a-z]+)\/search\/(.+)\.html" => "index.php?s=$1&c=search&rewrite=$2",  
     //【独立模块】模块搜索页(分页)（{modname}/search/{param}.html）
     "([a-z]+)\/search\.html" => "index.php?s=$1&c=search",  
     //【独立模块】模块搜索页（{modname}/search.html）


];