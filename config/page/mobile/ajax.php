<?php

/**
 * 自定义分页标签样式
 */

return array(
    'next_link' => '>',
    'prev_link' => '<',
    'last_link' => '>|',
    'first_link' => '|<',
    'cur_tag_open' => '<a class="ds-current">',
    'cur_tag_close' => '</a>',

);